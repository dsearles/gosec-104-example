# gosec G104 Example

This has been resolved in <https://github.com/securego/gosec/issues/671#issuecomment-897201033>!

This is a [Short, Self Contained, Correct (Compilable), Example](http://sscce.org/) of a difference between
version 2.8.0 and 2.8.1 with regards to surfacing [G104](https://securego.io/docs/rules/g104.html) errors.

## Difference

v2.8.0 finds a G104 error at
https://gitlab.com/dsearles/gosec-104-example/-/blob/a67ed9b719bc75cca2989ad0c4d8e3c6fa2cf9c4/main.go#L11 ,
while v2.8.1 does not find the error.

- This [test run](https://gitlab.com/dsearles/gosec-104-example/-/jobs/1454206595) that uses v2.8.0 finds the G104 error.
- This [test run](https://gitlab.com/dsearles/gosec-104-example/-/jobs/1454206596) that uses v2.8.1 does not find the G104 error.
